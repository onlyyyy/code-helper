import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: require('@/components/Home').default
    },
    {
      path: '/git',
      name: 'git',
      component: require('@/components/sub/Git').default
    },
    {
      path: '/hosts',
      name: 'hosts',
      component: require('@/components/sub/Hosts').default
    },
    {
      path: '/setup',
      name: 'setup',
      component: require('@/components/sub/Setup').default
    },
    {
      path: '/json',
      name: 'json',
      component: require('@/components/sub/Json').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
