import common from './common';
import * as CONSTANT from './constants';
import fs from 'fs';

export default new class {
    constructor() {
        
        this.setting = {};
    }

    getConfig () {
        let setting = {};
        if(fs.existsSync(CONSTANT.setup)) {
          setting = common.safeJsonParse(fs.readFileSync(CONSTANT.setup));
        }
        
        this.setting = setting;
    
    }
} 

