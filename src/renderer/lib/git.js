import common from './common';
import * as CONSTANT from './constants';
import { tools } from 'libcu';
const set = require('./set').default;
export default new class {
    constructor() {
        this.todoList = [];
        this.todoCheck();
    }
    //确认这个是不是git仓库
    checkGit(filePath) {
        try {
            let cmd = `cd ${filePath}; ${CONSTANT.commands.checkGitBase}`;
            console.log("命令为 ", cmd);

            let res = common.command(cmd);
            return common.trim(res);
        } catch (error) {
            console.log("error = ", error);
            if (error.toString().indexOf(CONSTANT.error.notGitBase) !== -1) {
                return false;
            } else {
                return common.trim(error.toString());
            }
        }

    }


    //获取所有的本地分支
    getLocalBranch(filePath) {
        try {

            let cmd = `cd ${filePath}; ${CONSTANT.commands.branch}`;
            console.log("获取分支的cmd为 ", cmd);
            let res = common.command(cmd);
            return {
                code: true,
                data: common.trim(res),
            }
        } catch (error) {
            console.log("error = ", error);
            return {
                code: false,
                data: common.trim(error.toString())


            }

        }
    }

    //解析git的分支
    parseGitBranch(branchList, filePath) {
        console.log("收到的数据 为 \n", branchList);
        //因为收到的数据不是数组 且带有分隔符
        branchList = branchList.split(CONSTANT.EOL);
        let res = [];
        for (let i = 0; i < branchList.length; i++) {
            res.push({
                index: i + 1,
                name: branchList[i],
                _checked: false,
                filePath
            })
        }
        return res;
    }


    setTodoList(list) {
        let repeatCount = 0;
        let todoCount = 0;
        for (let i = 0; i < list.length; i++) {

            if (common.contains(this.todoList, list[i], "branch")) {
                repeatCount++;
            } else {
                this.todoList.push(list[i]);
                todoCount++
            }
        }
        let response = '';
        let code = 0;
        //拼接文案
        if (todoCount == 0 && repeatCount == 0) {
            code = -1;
            response = '程序处理异常!';
        } else if (todoCount == 0 && repeatCount != 0) {
            response = `无需要处理的分支，存在${repeatCount}个需要监听的分支`;
            code = 1;
        } else if (todoCount != 0 && repeatCount == 0) {
            response = `已监听${todoCount}个分支`;
        } else if (todoCount != 0 && repeatCount != 0) {
            response = `已监听${todoCount}个分支,且存在${repeatCount}个需要监听的分支`;
            code = 1;
        } else {
            response = `其他错误!`;
            code= -1;
        }
        return {
            code ,
            data:response,
        };
    }

    //定期更新git的仓库
    todoCheck() {
        setInterval(() => {
            this.todoList.forEach(list => {
                this.pull(list);
            });
        }, (set.setting.interval || 10) * 1000);
    }

    pull(detail) {
        let cmd = `cd ${detail.path};git checkout ${detail.branch};git pull;`;
        try {
            let res = common.command(cmd);
            console.log('res :>> ', res);
        } catch (error) {
            console.log('error :>> ', error);
        }
       
    }



}

