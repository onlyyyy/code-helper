import os from 'os';

export const setup = './setup.json';

export const commands = {
    checkGitBase : 'git rev-parse --is-inside-work-tree;',
    branch:'git branch;',
    branchA:'git branch -a;',
    branchR:'git branch -r;',
    today:'git diff --shortstat "@{0 day ago}";'
}

export const error = {
    notGitBase:'fatal: not a git repository (or any of the parent directories): .git',
}

export const EOL = os.EOL;

export const HOSTS = '/etc/hosts';

export const HOSTS_NOT_PARSE = "无法解析的host数据";